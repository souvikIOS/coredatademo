//
//  DataModel.swift
//  CoreDataDemo
//
//  Created by Souvik on 09/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//

import Foundation


class PersonDetails{
    var fName : String?
    var lName : String?
    var age : Int?
    var gender : String?
    var mobile : String?
}
