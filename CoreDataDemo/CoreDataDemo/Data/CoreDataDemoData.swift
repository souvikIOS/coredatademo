//
//  CoreDataDemoData.swift
//  CoreDataDemo
//
//  Created by Souvik on 10/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//

import Foundation


class CoreDataDemoData: NSObject {
    
    func addPersonData(firstName : String, lastName : String, mobile : String, age : String, gender : String, completionHandler: @escaping (_ success : Bool, _ message : String) -> Void){
        if firstName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if lastName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if Utility.isValidMobileNumber(mobile){
                    if age.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                      if gender.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        //All requirement is satisfied
                        let objPerson = PersonDetails()
                        objPerson.fName = firstName
                        objPerson.lName = lastName
                        objPerson.mobile = mobile
                        objPerson.age = Int(age)
                        objPerson.gender = gender
                        CoreDataMethods.sharedInstance.savePersonDetail(objPerson: objPerson) { (success) in
                            if success{
                                DispatchQueue.main.async {
                                    completionHandler(true, "Person Details is Succeesfully Saved.")
                                }
                            }else{
                                completionHandler(false, "Sorry! Person Details is not added, Some error is there.")
                            }
                        }
                      }else{
                        completionHandler(false, "Please selcet gender")
                        }
                    }else{
                        completionHandler(false, "Please enter valid age")
                    }
                }else{
                    completionHandler(false, "Please enter a valid Mobile number")
                }
            }else{
                completionHandler(false, "Please enter Last Name")
            }
        }else{
            completionHandler(false, "Please enter First Name")
        }

    }
    
    
    func getAllPersonInfo() -> [PersonDetails]{
        return CoreDataMethods.sharedInstance.getAllPersonData()
    }
    
    
    func deletePersonInfo(objPerson : PersonDetails,completionHandler: @escaping (_ success : Bool, _ message : String) -> Void){
        CoreDataMethods.sharedInstance.deleteSinglePersonInfo(objPerson: objPerson) { (status, message) in
            completionHandler(status, message)
        }
    }
}
