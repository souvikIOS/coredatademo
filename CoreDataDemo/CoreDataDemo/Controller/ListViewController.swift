//
//  ListViewController.swift
//  CoreDataDemo
//
//  Created by Souvik on 09/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var arrAllPersons : [PersonDetails] = []
    let dataModel = CoreDataDemoData()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.arrAllPersons = dataModel.getAllPersonInfo()
        self.tableView.reloadData()
    }


}


extension ListViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrAllPersons.count > 0{
            self.tableView.restore()
        }else{
            self.tableView.setEmptyMessage("No Data Found")
        }
        return self.arrAllPersons.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PersonCell.identifier, for: indexPath) as! PersonCell
        cell.personData = self.arrAllPersons[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "personDetailsTableViewController") as! PersonDetailsTableViewController
        vc.objPerson = self.arrAllPersons[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
