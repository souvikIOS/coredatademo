//
//  PersonDetailsTableViewController.swift
//  CoreDataDemo
//
//  Created by Souvik on 10/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//

import UIKit

class PersonDetailsTableViewController: UITableViewController {
    @IBOutlet weak var textFieldFName: UITextField!
    @IBOutlet weak var textFieldLName: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldAge: UITextField!
    @IBOutlet weak var textFieldGender: UITextField!
    var arrGender = ["Male", "Female", "Other"]
    var genderPicker : UIPickerView?
    var objPerson : PersonDetails?
    let dataModel = CoreDataDemoData()

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.shoPersonDetails()
        self.createGenderPicker()
    }
    
    //MARK: - IBActions - 
    
    @IBAction func btnBackDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnUpdateDidClick(_ sender: Any) {
        dataModel.addPersonData(firstName: self.textFieldFName.text!, lastName: self.textFieldLName.text!, mobile: self.textFieldPhone.text!, age: textFieldAge.text!, gender: textFieldGender.text!) { (status, message) in
            DispatchQueue.main.async {
                if status{
                    self.showAlertWith(message: message) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    self.showAlert(message: message)

                }
            }
        }

    }
    
    @IBAction func btnDeleteDidClick(_ sender: Any) {
        dataModel.deletePersonInfo(objPerson: self.objPerson!) { (status, message) in
            DispatchQueue.main.async {
                if status{
                    self.showAlertWith(message: message) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    self.showAlert(message: message)
                    
                }
            }
            
        }
    }
    

}


// MARK: - Self Methods -
extension PersonDetailsTableViewController{
    func shoPersonDetails(){
        self.textFieldFName.text = objPerson?.fName
        self.textFieldLName.text = objPerson?.lName
        self.textFieldAge.text = "\(objPerson?.age ?? 0)"
        self.textFieldGender.text = objPerson?.gender
        self.textFieldPhone.text = objPerson?.mobile
    }
    
    
      func createGenderPicker()
      {
        
        genderPicker = UIPickerView(frame: CGRect(x: 0, y: self.view.bounds.height - 216, width: self.view.frame.width, height: 216))
        genderPicker?.backgroundColor = .white
        genderPicker?.dataSource = self
        genderPicker?.delegate = self
        textFieldGender.inputView = genderPicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor.darkGray
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textFieldGender.inputAccessoryView = view_Accessory
        
        
    }
      
      
      
      @objc func selectionDone(_ sender: UIButton ) {
          textFieldGender.text = String(describing: self.arrGender[(genderPicker?.selectedRow(inComponent: 0))!])
          self.textFieldGender.resignFirstResponder()

      }
      
      @objc func selectionCancel(_ sender: UIButton ) {
        self.textFieldGender.text = ""
        self.textFieldGender.resignFirstResponder()
      }
      
    
}


extension PersonDetailsTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrGender.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(describing: self.arrGender[row])

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textFieldGender.text = String(describing: self.arrGender[row])
    }

}
