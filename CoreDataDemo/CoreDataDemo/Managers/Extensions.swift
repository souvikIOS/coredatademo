//
//  Extensions.swift
//  CoreDataDemo
//
//  Created by Souvik on 09/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//
import Foundation
import UIKit

enum ViewSide {
    case Left, Right, Top, Bottom
}
enum VerticalLocation: String {
    case bottom
    case top
}
let usedFont = UIFont(name: "HelveticaNeue-Regular", size: 18.0)
extension UIView {
    
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        self.layer.addSublayer(border)
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    

}

extension UITableViewCell{
    static var identifier:String{
        return String(describing : self)
    }
}


extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 18.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}

struct Utility {
    //validate password
    public static func isValidMobileNumber(_ mobile: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,11}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let isValidPhone = phoneTest.evaluate(with: mobile)
        return isValidPhone
    }
}



extension UIViewController {
    
    
    
    func showAlert(message : String){
        let alertMessage = UIAlertController(title: "Alert!", message: message, preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "Ok", style: .destructive) { (alt) in
        }
        alertMessage.view.tintColor = UIColor.darkGray
        alertMessage.addAction(okAction)
        self.present(alertMessage, animated: true, completion: nil)
    }
    
    func showAlertWith(message : String, okAction : @escaping () -> Void){
         let alertMessage = UIAlertController(title: "Alert!", message: message, preferredStyle: .actionSheet)
         let ok = UIAlertAction(title: "Ok", style: .destructive) { (alt) in
             okAction()
         }
        alertMessage.view.tintColor = UIColor.darkGray
         alertMessage.addAction(ok)
         alertMessage.dismiss(animated: true, completion: {
             self.present(alertMessage, animated: true, completion: nil)

         })

     }
     
}
