//
//  CoreDataMethods.swift
//  CoreDataDemo
//
//  Created by Souvik on 09/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//
import Foundation
import CoreData
import UIKit

final class CoreDataMethods {
    
    static let sharedInstance = CoreDataMethods()
    fileprivate let appDelegate = UIApplication.shared.delegate as! AppDelegate
    fileprivate let context: NSManagedObjectContext
    var chatHistotyNumberOfDataPerPage: Int = 30
    
    
    var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "CoreDataDemo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    private init() {
        
        if #available(iOS 10.0, *) {
            self.context = self.persistentContainer.viewContext
        }
        else {
            self.context = self.persistentContainer.viewContext
        }
        
    }
    
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    public func savePersonDetail(objPerson : PersonDetails, completionHandler: @escaping (_ success : Bool) -> Void){
            //check if chat already exists or not
            let fetchRequest = NSFetchRequest<Person>(entityName: "Person")

        fetchRequest.predicate = NSPredicate(format: "mobile=%@", objPerson.mobile!)
            do
            {
                let fetchResults = try self.context.fetch(fetchRequest)
                if fetchResults.count > 0 {
                    fetchResults[0].firstName = objPerson.fName
                    fetchResults[0].lastName = objPerson.lName
                    fetchResults[0].age = Int32(objPerson.age ?? 0)
                    fetchResults[0].gender = objPerson.gender
                }else {
                    //not exists (i.e - insert)
                    let newObjectToInsert: Person = NSEntityDescription.insertNewObject(forEntityName: "Person", into: self.context) as! Person
                    newObjectToInsert.firstName = objPerson.fName
                    newObjectToInsert.lastName = objPerson.lName
                    newObjectToInsert.age = Int32(objPerson.age ?? 0)
                    newObjectToInsert.gender = objPerson.gender
                    newObjectToInsert.mobile = objPerson.mobile

                }
            }
            catch {
                print("error in fetching data")
                completionHandler(false)
            }
        if context.hasChanges {
            do {
                try context.save()
                completionHandler(true)

            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                completionHandler(false)
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")

            }
        }

    }
    
        
    public func getAllPersonData() -> [PersonDetails] {
        var arrPerson : [PersonDetails] = []
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        do
        {
            let fetchResults = try self.context.fetch(fetchRequest)
            for item in fetchResults{
                let objPerson = PersonDetails()
                objPerson.fName = item.firstName!
                objPerson.lName = item.lastName!
                objPerson.age = Int(item.age)
                objPerson.mobile = item.mobile
                objPerson.gender = item.gender
                arrPerson.append(objPerson)
            }
            return arrPerson
        }
        catch {
            print("error in fetching chat data")
            return arrPerson
        }
        
    }
    
    public func deleteSinglePersonInfo(objPerson : PersonDetails, completionHandler: @escaping (_ success : Bool, _ message : String) -> Void){
        //check if Order already exists or not
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        
        fetchRequest.predicate = NSPredicate(format: "mobile=%@", objPerson.mobile!)
        do
        {
            let fetchResults = try self.context.fetch(fetchRequest)
            if fetchResults.count > 0 {
                for obj in fetchResults {
                    context.delete(obj)
                }
            }
            
        }
        catch {
            print("error in fetching data")
            completionHandler(false, "error in fetching data")
        }
        if context.hasChanges {
            do {
                try context.save()
                completionHandler(true, "Person Info deleted successfully")
                
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError

                completionHandler(false, "Unresolved error \(nserror), \(nserror.userInfo)")
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                
            }
        }
        
    }
    
    
}
