//
//  PersonCell.swift
//  CoreDataDemo
//
//  Created by Souvik on 09/01/21.
//  Copyright © 2021 Souvik. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelMobile: UILabel!
    @IBOutlet weak var labelAge: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var personData : PersonDetails?{
        didSet{
            self.labelName.text = "\(personData?.fName ?? "") \(personData?.lName ?? "") (\(personData?.gender ?? ""))"
            self.labelMobile.text = "Phone : \(personData?.mobile ?? "")"
            self.labelAge.text = "Age : \(personData?.age ?? 0)"
        }
    }

}
